package lab3infracom;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.event.ListSelectionEvent;

import jdk.management.resource.internal.TotalResourceContext;

public class Cliente {
	protected Socket client;
	protected BufferedReader in;
	public BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
	private String ruta;
	private int fileSize;
	private boolean recibir; 
	private long startTime;
	
	private int totalRead = 0;
	
	private long stopTime = 0;
	public static void main(String[] args) {
		new Cliente("192.168.1.119", 1978);
	}
	public Cliente(String hostName, int ip) {
		try {

			this.client = new Socket(hostName, ip);
			
			this.in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
			
			OutputStream outputStream = client.getOutputStream();
			PrintWriter out = new PrintWriter(outputStream,true);
			String buffer = null;
			out.println("Ready to receive a file");
			out.flush();
			buffer = in.readLine();
			while (buffer != null && !client.isClosed()) {
				// receive file
				System.out.println(buffer);
				if(buffer.startsWith("INFO:"))
				{
					ruta = buffer.split("INFO:")[1].split(",")[0];
					fileSize = Integer.parseInt(buffer.split("INFO:")[1].split(",")[1]);
					System.out.println("INFO CLiente");
					System.out.println("TAMAÑO: "+fileSize);
					System.out.println("RUTA: "+ruta);
					out.println("Ready to start process");
					out.flush();
					recibir =true;
					
				}
				else if(buffer.startsWith("TIME:")) {
					String startTimeStr = buffer.split("TIME:")[1];
					startTime = Long.parseLong(startTimeStr);					
					
				
					
				}
				if(recibir && startTime != 0) {
					String checksum = saveFile(client, fileSize, out);
					out.println("Archivo recibido:" + checksum);
					out.flush();
					
					out.println("Paquetes:" + totalRead);
					out.flush();
					
					out.println("QUIT");
					out.flush();
					recibir = false;
					client.close();

				}
				if(!client.isClosed()) {
					buffer = in.readLine();
				}
				else buffer = null;
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String saveFile(Socket clientSock, int filesize, PrintWriter out) throws IOException {
		DataInputStream dis = new DataInputStream(clientSock.getInputStream());
		String ruta = "./enviados/copia.pptx";
		FileOutputStream fos = new FileOutputStream(ruta);
		byte[] buffer = new byte[4096];
		
		int read = 0;
		int remaining = filesize;
		while((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += read;
			remaining -= read;
//			System.out.println("read " + totalRead + " bytes.");
			fos.write(buffer, 0, read);
		}
		
		stopTime = System.currentTimeMillis();
		double milis = stopTime - startTime;
		System.out.println("Start " + startTime);
		System.out.println("Stop " + stopTime);
		System.out.println("Tiempo de ejecuci�n: " + milis/1000 + " segundos.");
		
		out.println("Tiempo total:" + milis/1000);
		out.flush();
		
		String hex  = "";
		try {

			MessageDigest md = MessageDigest.getInstance("SHA-256"); //SHA, MD2, MD5, SHA-256, SHA-384...
			hex = checksum(ruta, md);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
//		fos.close();
//		dis.close();
		return hex;
		
		
	}
	
	public static byte[] concat(byte[] b, byte[] temporal) {
		byte[] result = Arrays.copyOf(b, b.length + temporal.length);
		System.arraycopy(temporal, 0, result, b.length, temporal.length);
		return result;
	}

	private static String checksum(String filepath, MessageDigest md) throws IOException {

		// file hashing with DigestInputStream
		try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
			while (dis.read() != -1) ; //empty loop to clear the data
			md = dis.getMessageDigest();
		}

		// bytes to hex
		StringBuilder result = new StringBuilder();
		for (byte b : md.digest()) {
			result.append(String.format("%02x", b));
		}
		return result.toString();

	}
}